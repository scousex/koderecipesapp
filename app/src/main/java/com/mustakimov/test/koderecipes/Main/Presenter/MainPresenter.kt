package com.mustakimov.test.koderecipes.Main.Presenter

import android.support.v4.app.Fragment
import com.mustakimov.test.koderecipes.Main.View.MainView

interface MainPresenter{

    fun attachView(mv:MainView)
    fun changeFragment(fragment: Fragment)

}