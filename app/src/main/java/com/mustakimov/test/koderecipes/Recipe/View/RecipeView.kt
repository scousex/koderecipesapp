package com.mustakimov.test.koderecipes.Recipe.View

import com.mustakimov.test.koderecipes.RecipesList.Model.RecipesListItem

interface RecipeView{
    fun setRecipeData(data: RecipesListItem)
}