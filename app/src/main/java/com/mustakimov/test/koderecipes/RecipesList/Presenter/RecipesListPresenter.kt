package com.mustakimov.test.koderecipes.RecipesList.Presenter

import android.view.View

interface RecipesListPresenter{

    fun loadRecipes() : Boolean
    fun OnListItemClick(v: View, position: Int)
    fun bindAdapter()

}