package com.mustakimov.test.koderecipes.SimpleImplementation

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.FrameLayout
import com.mustakimov.test.koderecipes.R

class ListActivity : AppCompatActivity(){


    private lateinit var container: FrameLayout


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        container = this.findViewById(R.id.container) as FrameLayout

    }

}
