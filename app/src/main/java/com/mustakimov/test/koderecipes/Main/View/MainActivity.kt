package com.mustakimov.test.koderecipes.Main.View

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import android.widget.FrameLayout
import com.mustakimov.test.koderecipes.Main.Presenter.MainPresenter
import com.mustakimov.test.koderecipes.R

class MainActivity : AppCompatActivity() , MainView{

    private lateinit var mainPresenter: MainPresenter
    private lateinit var container: FrameLayout
    private lateinit var fragmentManager: FragmentManager
    private lateinit var fragmentTransaction: FragmentTransaction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        container = this.findViewById(R.id.container) as FrameLayout
        fragmentManager = this.supportFragmentManager
        fragmentTransaction = fragmentManager.beginTransaction()


    }
    override fun showFragment(fragment: Fragment){
        fragmentTransaction.add(R.id.container,fragment)
        fragmentTransaction.commit()
    }
}
