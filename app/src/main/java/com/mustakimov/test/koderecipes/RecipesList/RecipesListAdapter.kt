package com.mustakimov.test.koderecipes.RecipesList


import android.content.Context
import android.support.annotation.NonNull
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mustakimov.test.koderecipes.R
import com.mustakimov.test.koderecipes.RecipesList.Model.RecipesListItem
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject




class RecipesListAdapter(private val ctx: Context,
                         val recipes:ArrayList<RecipesListItem>): RecyclerView.Adapter<ViewHolder>() {


    val imgLoader: ImageLoader = ImageLoader.getInstance()
    val onClickSubject: PublishSubject<RecipesListItem> = PublishSubject.create()



    override fun onCreateViewHolder(@NonNull parent: ViewGroup, viewType: Int): ViewHolder {
        imgLoader.init(ImageLoaderConfiguration.createDefault(ctx))
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_layout,parent, false)
        val viewHolder =
                ViewHolder(view, object : ViewHolder.OnRecipeClickListener {
                    override fun onItemClick(view: View) {

                    }
                })
        return viewHolder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvName.text = recipes.get(position).name
        holder.tvDescription.text = recipes.get(position).description
        val recipe = recipes.get(position)

        imgLoader.displayImage(recipes.get(position).images[0],holder.ivImage)

        holder.itemView.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                onClickSubject.onNext(recipe)
            }
        })
    }

    fun getItem(position: Int) = recipes.get(position).uuid

    fun getPositionClicks() = onClickSubject as Observable<RecipesListItem>

    override fun getItemCount() = recipes.size

    fun getRecipeId(position: Int) = recipes[position].uuid






}