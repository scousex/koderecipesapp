package com.mustakimov.test.koderecipes.RecipesList.Model

import com.google.gson.annotations.SerializedName

class RecipesList(@SerializedName ("recipes")
                   val recipesList:ArrayList<RecipesListItem>){
    fun get(index:Int) = recipesList[index]
    fun count() = recipesList.count()

}