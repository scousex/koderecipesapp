package com.mustakimov.test.koderecipes.Main.View

import android.support.v4.app.Fragment

interface MainView{
    fun showFragment(fragment: Fragment)
}