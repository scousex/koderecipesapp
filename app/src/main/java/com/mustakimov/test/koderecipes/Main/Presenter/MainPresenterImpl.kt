package com.mustakimov.test.koderecipes.Main.Presenter

import android.support.v4.app.Fragment
import com.mustakimov.test.koderecipes.Main.View.MainView
import com.mustakimov.test.koderecipes.RecipesList.View.RecipesListFragment

class MainPresenterImpl : MainPresenter{


    private lateinit var recipesListFragment: RecipesListFragment
    private var mainView:MainView? = null

    override fun attachView(mv: MainView) {
            mainView = mv
            mv!!.showFragment(recipesListFragment!!)
    }

    override fun changeFragment(fragment: Fragment) {
        mainView!!.showFragment(fragment)
    }

}