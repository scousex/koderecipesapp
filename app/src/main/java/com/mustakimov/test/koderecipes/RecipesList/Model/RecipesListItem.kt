package com.mustakimov.test.koderecipes.RecipesList.Model

class RecipesListItem( val uuid:String,val name:String,val images: Array<String>, val lastUpdated: Int,
                        val description:String, val instructions:String, val difficulty:String){

}