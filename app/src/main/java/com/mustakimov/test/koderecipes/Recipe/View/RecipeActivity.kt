package com.mustakimov.test.koderecipes.Recipe.View

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.mustakimov.test.koderecipes.Api.RecipesApiCreator
import com.mustakimov.test.koderecipes.RecipesList.Model.RecipesListItem
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class RecipeActivity : AppCompatActivity(), RecipeView {

    private var listener: OnRecipeSelectedListener? = null
    private lateinit var tvName: TextView
    private lateinit var tvDescription: TextView
    private lateinit var tvInstructions: TextView
    private lateinit var layoutImages: LinearLayout
    private lateinit var ivImages: ArrayList<ImageView>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    interface OnRecipeSelectedListener{
        fun onRecipeSelected(recipeId:String)
    }

    fun provideRecipeData(recipeId: String){
        val api = RecipesApiCreator.instance()
        api.getRecipe(recipeId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::setRecipeData)
    }

    override fun setRecipeData(data: RecipesListItem){
        tvName.text = data.name
        tvDescription.text = data.description
        tvInstructions.text = data.instructions
    }




}
