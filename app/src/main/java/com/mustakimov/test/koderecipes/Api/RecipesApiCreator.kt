package com.mustakimov.test.koderecipes.Api

object RecipesApiCreator{
    val api = RecipesApi.create()
    fun instance(): RecipesApi {
        return api
    }
}