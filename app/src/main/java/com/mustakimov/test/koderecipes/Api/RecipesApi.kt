package com.mustakimov.test.koderecipes.Api

import com.mustakimov.test.koderecipes.RecipesList.Model.RecipesList
import com.mustakimov.test.koderecipes.RecipesList.Model.RecipesListItem
import io.reactivex.Observable
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import java.util.concurrent.TimeUnit


interface RecipesApi{
    @GET("recipes")
        fun getRecipes(): Observable<RecipesList>
    @GET("recipes/{uuid}")
        fun getRecipe(@Path("uuid") uuid:String):Observable<RecipesListItem>

    companion object Factory{
        fun create(): RecipesApi {
            val okHttpClient:OkHttpClient =
            OkHttpClient.Builder()
                    .connectTimeout(60,TimeUnit.SECONDS)
                    .writeTimeout(60,TimeUnit.SECONDS)
                    .readTimeout(60,TimeUnit.SECONDS)
                    .build()
            val retrofit = Retrofit.Builder()
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .baseUrl("https://test.kode-t.ru/")
                    .build()
            return retrofit.create(RecipesApi::class.java)
        }
    }
}