package com.mustakimov.test.koderecipes.Recipe.Presenter

interface RecipePresenter{

    fun provideRecipeData(recipeId: String)

}