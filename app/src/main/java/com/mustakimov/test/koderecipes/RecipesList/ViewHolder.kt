package com.mustakimov.test.koderecipes.RecipesList

import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.item_layout.view.*

class ViewHolder(val view: View,
                 val listener: OnRecipeClickListener) : RecyclerView.ViewHolder(view), View.OnClickListener {

    val tvName = view.tvName
    val tvDescription = view.tvDescr
    val ivImage = view.ivImage

    init{
        view.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        listener.onItemClick(v!!)
    }

    interface OnRecipeClickListener {
        fun onItemClick(view: View)

    }
}